<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sign Up Page</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <p><label for="firstname">First Name:</label></p>
        <input type="text" name="firstname" id="firstname" />
        <p><label for="lastname">Last Name:</label></p>
        <input type="text" name="lastname" id="lastname" required />

        <p><label for="email">Email</label>
            <input type="email" name="email" id="email">
        </p>

        <p>Gender:</p>
        <p>
            <input type="radio" name="gender" id="male" checked />
            <label for="male">Male</label>
        </p>
        <p>
            <input type="radio" name="gender" id="female" />
            <label for="female">Female</label>
        </p>
        <p>
            <input type="radio" name="gender" id="other_gender" />
            <label for="other_gender">Other</label>
        </p>

        <p>Nasionality:</p>
        <select name="nationality" id="nationality">
            <option value="indonesian" selected>Indonesian</option>
            <option value="malaysian">Malaysian</option>
            <option value="european">European</option>
            <option value="singapuran">Singapura</option>
        </select>

        <p>Language Spoken:</p>
        <p>
            <input type="checkbox" name="bahasaindonesia" id="bahasaindonesia" checked /><label
                for="bahasaindonesia">Bahasa Indonesia</label>
        </p>
        <p>
            <input type="checkbox" name="english" id="english" /><label for="english">English</label>
        </p>
        <p>
            <input type="checkbox" name="other_languange" id="other_languange" /><label
                for="other_languange">Other</label>
        </p>

        <p><label for="bio">Bio</label></p>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>

        <p>
            <button type="submit">Sign up</button>
            <button type="submit"><a href="/">Back Home</a></button>
        </p>
    </form>
</body>

</html>